from typesinfo import Context, SharingMode
import macros

proc splitSettingsExpr*(node: NimNode): tuple[t: NimNode, s: NimNode]
  {.compileTime.}
proc applyOptions*(context: Context,
                   options: NimNode | seq[NimNode]): Context {.compileTime.}

from strutils import cmpIgnoreStyle

proc getBoolean(value_node: string, name: string): bool =
  let code = (
    2*int(cmpIgnoreStyle(value_node, "true") == 0) +
    int(cmpIgnoreStyle(value_node, "false") == 0)
  )
  case code
  of 0: error("The property " & name & " can be only 'true' or 'false' but not" & value_node)
  of 1: return false
  of 2: return true
  else: error("Unexpected error! " & name & " is in superposition! WTF?")

proc applyOptions(context: Context, options: NimNode | seq[NimNode]): Context =
  ## Applies given options to the `context` and returns it without changing
  ## the original one.
  ## Options should be a NimNode or seq[NimNode] which contains nnkExprColonExpr
  ## nodes with key - value pairs.
  result = context
  let pragma = type(options) is NimNode and options.kind == nnkPragma
  for option in options.items():
    if pragma and option.kind != nnkExprColonExpr:
      continue
    option.expectKind(nnkExprColonExpr)
    option.expectMinLen(2)
    let key = option[0].repr
    let val = option[1].repr
    case key
    of "endian":
      result.swapEndian = cmpIgnoreStyle(val, $cpuEndian) != 0
    of "dynamic":
      result.is_static = not getBoolean(val, key)
    of "size":
      result.overrides.size.insert((option[1], context.depth), 0)
    of "sizeof":
      result.overrides.sizeof.add((option[1], context.depth))
    of "shared":
      result.sharing_mode = if getBoolean(val, key): SharingMode.Export else: SharingMode.Local
    elif pragma:
      continue
    else:
      error("Unknown setting: " & key)

proc splitSettingsExpr(node: NimNode): tuple[t: NimNode, s: NimNode] =
  ## Checks if given ``node`` is the settings expression.
  ## If so, returns original type declaration and settings node.
  ## Otherwise returns node and empty TableConstr node.
  if node.kind == nnkInfix and node.len == 3 and $node[0] == "as" and
     node[2].kind == nnkTableConstr:
    return (t: node[1], s: node[2])
  else:
    return (t: node, s: newNimNode(nnkTableConstr))
